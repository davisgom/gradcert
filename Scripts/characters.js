function shuffle(array) {
  array.sort(() => Math.random() - 0.5);
}

let characters = [
  "Austen",
  "Bryce",
  "Carson",
  "Darcy",
  "Ellis",
  "Faye",
  "Gael",
  "Hollis",
  "Inez",
  "Jaden",
  "Kai",
  "Linden"
];
shuffle(characters);

var elements = document.getElementsByClassName("character");

var len = characters.length;
for (i = 0; i < elements.length; i++) {
    elements[i].className += ' ' + characters[i%len];
}
