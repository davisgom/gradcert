<div class="mb-30">
	<a href="competencies">
		<i class="fa fa-arrow-circle-left"></i> Back to Competencies
	</a>
</div>

<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?></h1>

<h2>
	Foundations and Variations
  <span class="small d-block d-lg-inline">(All Required)</span>
</h2>

<ol>
	<li>
		History of Community-Engaged Scholarship
  </li>

  <li value="2">
		Variations in Community-Engaged Scholarship
  </li>
</ol>

<h2>
	Community Partnerships
  <span class="small d-block d-lg-inline">(All Required)</span>
</h2>

<ol>
	<li value="3">
		Initiating Community Partnerships
  </li>

  <li value="4">
		Sustaining Community Partnerships
  </li>

	<li value="5">
		Techniques for Community Collaboration
  </li>
</ol>

<h2>
	Critical Reflection and Critical Thinking
  <span class="small d-block d-lg-inline">(All Required)</span>
</h2>

<ol>
	<li value="6">
		Engaging with Diverse Communities
  </li>

	<li value="7">
		Critical Reflection and Critical Thinking
  </li>

	<li value="8">
		Ethics in Community-Engaged Scholarship
  </li>
</ol>

<h2>
	Community-Engaged Scholarship and Practice
  <span class="small d-block d-lg-inline">(Select ONLY ONE)</span>
</h2>

<ol>
	<li value="9">
		Community-Engaged Research and Creative Activities
  </li>

  <li value="10">
		Community-Engaged Teaching and Learning
  </li>

  <li value="11">
		Community-Engaged Service and Practice
  </li>
</ol>

<h2>
	Approaches and Perspectives
  <span class="small d-block d-lg-inline">(Select ONLY ONE)</span>
</h2>

<ol>
	<li value="12">
		Asset Based Community Engagement
  </li>

	<li value="13">
		Capacity Building Approaches to Sustained Change
  </li>

  <li value="14">
		Coalition or Systems Approaches to Community Change
  </li>
</ol>

<h2>
	Evaluation and Assessment
  <span class="small d-block d-lg-inline">(All Required)</span>
</h2>

<ol>
	<li value="15">
		Evaluating Community Partnerships
  </li>

  <li value="16">
		Quality, Excellence, and Rigor in Peer Review of Community-Engaged Scholarship
  </li>
</ol>

<h2>
	Communication and Scholarly Skills
  <span class="small d-block d-lg-inline">(All Required)</span>
</h2>

<ol>
	<li value="17">
		Communicating with Public Audiences
  </li>

	<li value="18">
		Communicating with Academic Audiences
  </li>
</ol>

<h2>
	Successful Community Engagement Careers
  <span class="small d-block d-lg-inline">(All Required)</span>
</h2>

<ol>
	<li value="19">
		Documenting and Communicating Your Engagement Accomplishments<a href="#note" id="note1" title="More information">*</a>
  </li>

  <li value="20">
		Community Engagement Across the Career Span
  </li>
</ol>

<p id="note">
	<strong>NOTE</strong>: Competency #19—Documenting and Communicating Your Engagement Accomplishments—is fulfilled by your written portfolio and presentation. You do not need to address this separately in your portfolio.
</p>
