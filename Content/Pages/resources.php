<div class="pt-40 pb-30 pb-md-30">
  <?php include("Views/Shared/Partials/page-banner.php"); ?>
</div>

<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?></h1>

<br />

<h2>
	Engagement Scholarship Consortium
  <span class="small">(ESC)</span>
</h2>

<p>
	<a href="http://engagementscholarship.org/" <?php echo $external; ?> target="_blank">
    http://engagementscholarship.org
  </a>
</p>

<ul>
	<li>
		focuses on community partnerships, community engagement, particularly at land-grant and public universities
  </li>

  <li>
		is associated with the national Macgrath awards for faculty community engagement
  </li>

	<li>
		hosts the Emerging Engagement Scholars Workshop <span class="small">(EESW)</span>
  </li>

  <li>
		coordinates a national conference every fall
  </li>
</ul>



<hr class="divider">

<h2>
	International Association for Research on Service Learning and Community Engagement
  <span class="small">(IARSLCE)</span>
</h2>

<p>
	<a href="http://www.researchslce.org/" <?php echo $external; ?> target="_blank">
    http://www.researchslce.org
  </a>
</p>

<ul>
	<li>
		focuses on research about K-12 service learning, higher education service learning, faculty roles &amp; rewards about community engagement
  </li>

  <li>
		publishes <em>International Journal on Research on Service Learning and Community Engagement,</em> an on-line, peer reviewed journal
  </li>

  <li>
		gives national community engagement awards for graduate students and faculty
  </li>

  <li>
		supports a Graduate Student Network that pairs students with mentors
  </li>

  <li>
		coordinates national conference every fall
  </li>

  <li>
		has a Facebook group for women in SLCE
  </li>
</ul>



<hr class="divider">

<h2>
	Imagining America: Artists &amp; Scholars in Public Life
  <span class="small">(IA)</span>
</h2>

<p>
	<a href="http://imaginingamerica.org/" <?php echo $external; ?> target="_blank">
    http://imaginingamerica.org
  </a>
</p>

<ul>
	<li>
		focuses public engagement in the arts, humanities, and design fields
  </li>

  <li>
		publishes <em>Public,</em> an on-line, peer reviewed journal
  </li>

	<li>
		supports PAGE (Publicly Active Graduate Student) Fellows network
  </li>

  <li>
		coordinates a national conference every fall
  </li>
</ul>



<hr class="divider">

<h2>
	Community-Campus Partnerships for Health
  <span class="small">(CCPH)</span>
</h2>

<p>
	<a href="https://www.ccphealth.org/" <?php echo $external; ?> target="_blank">
    https://www.ccphealth.org
  </a>
</p>

<ul>
	<li>
		focuses on healthy people and communities, with health broadly defined
  </li>

  <li>
		supports community-based, participatory research approaches and strong community partnerships, including those with a social justice mission
  </li>

  <li>
		has multiple listservs, including some with grant, fellowship, and job opportunities
  </li>

  <li>
		coordinates annual conference
  </li>

  <li>
		supports CES4Health.Info, a peer review mechanism for non-traditional scholarly products
  </li>
</ul>



<hr class="divider">

<h2>
	Association for Community Organization and Social Action
  <span class="small">(ACOSA)</span>
</h2>

<p>
	<a href="https://acosa.clubexpress.com/content.aspx?page_id=0&amp;club_id=789392" <?php echo $external; ?> target="_blank">
    https://acosa.clubexpress.com/content.aspx?page_id=0&amp;club_id=789392
  </a>
</p>

<ul>
	<li>
		For community organizers, activists, nonprofit administrators, community builders, policy practitioners, students, and educators
  </li>

  <li>
		Has a focus on social work, particularly the macro (non-clinical) focus
  </li>

  <li>
		Supports the <em>Journal of Community Practice</em>
  </li>

  <li>
		Gives awards to graduate students and faculty
  </li>
</ul>



<hr class="divider">

<h2>
	National Alliance for Broader Impacts
  <span class="small">(NABI)</span>
</h2>

<p>
	<a href="http://broaderimpacts.net/" <?php echo $external; ?> target="_blank">
    http://broaderimpacts.net
  </a>
</p>

<ul>
	<li>
		For faculty and engagement/outreach professionals who need to learn about how to write up the “broader impact” section of National Science Foundation grants
  </li>

  <li>
		Showcases dissemination results of community engaged research to public audiences
  </li>

  <li>
		Has a focus on Science Technology Engineering and Mathematics (STEM) fields
  </li>

  <li>
		Hosts an annual conference in the spring
  </li>
</ul>



<hr class="divider">

<h2>
	Campus Compact
</h2>

<p>
	<a href="http://compact.org/" <?php echo $external; ?> target="_blank">
    http://compact.org
  </a>
</p>

<ul>
	<li>
		Most states have their own chapter
  </li>

  <li>
		For faculty and staff interested in service-learning, civic engagement
  </li>

  <li>
		Archives toolboxes, syllabi, knowledge hubs, &amp; other publications on website
  </li>

  <li>
		Hosts conferences, grants, and awards
  </li>
</ul>



<hr class="divider">

<h2>
	Michigan Campus Compact
</h2>

<p>
	<a href="http://micampuscompact.org/" <?php echo $external; ?> target="_blank">
    http://micampuscompact.org
  </a>
</p>

<ul>
	<li>
		works with Michigan colleges and universities to promote best practices for service-learning and civic engagement
  </li>

  <li>
		holds annual conference, summits, and other professional development opportunities
  </li>

  <li>
		coordinates a grants programs
  </li>

	<li>
		organizes annual awards program for students, faculty, and institutions
  </li>
</ul>
