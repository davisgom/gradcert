<?php include("Views/Shared/Partials/requirements-nav.php") ?>

<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?></h1>

<h2>
	Community Portfolio <span class="small d-block d-lg-inline">(Third Requirement)</span>
</h2>

<p>
	The third requirement for the Graduate Certification in Community Engagement is the written engagement portfolio and presentation. Preparing an engagement portfolio is an opportunity for students to:
</p>

<ul>
	<li>
		Reflect on the scholarship and practice of community engagement
  </li>

	<li>
		Document their community-engaged scholarship methodically, including processes, outcomes, and evidence related to their collaboration with community partners
  </li>

	<li>
		Solicit critical feedback from community partners and faculty mentors on their perspectives about the collaboration
  </li>

	<li>
		Gather new and supporting materials to present for peer review
  </li>

	<li>
		Generate new insights through reflective writing
  </li>

	<li>
		Practice talking about their community-engaged scholarship or practice
  </li>
</ul>

<p>
	The engagement portfolio is composed of two parts: written portfolio and portfolio presentation.
</p>

<p>
	<strong>For the written portfolio, students must:</strong>
</p>

<ul>
	<li>
		Demonstrate mastery of core engagement competencies
  </li>

	<li>
		Document their mentored community engagement experience
  </li>

	<li>
		Include their community partners' and mentor's perspectives on their collaboration experience and skills
  </li>

	<li>
		Support their reflections with additional materials and evidence as appendices
  </li>

	<li>
		Focus on breadth and comprehensiveness, addressing 15 of the 20 competencies
  </li>
</ul>

<p>
	<strong>For the portfolio presentation, students must:</strong></p>

<ul>
	<li>
		Tell their personal engagement story
  </li>

	<li>
		Support their reflections with documentation about their experiences
  </li>

	<li>
		Share their lessons learned and ideas about future community engagement
  </li>

	<li>
		Focus on depth, by discussing two competencies that were particularly meaningful to them
  </li>
</ul>

<p>
	Portfolio presentations take place at the end of each semester, during the summer, or at ad-hoc times throughout the year. Written portfolios are due one week before the presentation date. The program coordinator works with students to schedule portfolio presentations when University Outreach and Engagement faculty and staff are available.
</p>

<div class="pt-80 pb-40">
  <?php include("Views/Shared/Partials/page-banner.php"); ?>
</div>

<h2>
	Purpose of Written Portfolio and Presentation
</h2>

<p>
	Your written portfolio and presentation is the third and final requirement for the MSU Graduate Certification in Community Engagement. Preparing and presenting your portfolio are opportunities to
</p>

<ul>
	<li>
		Describe and honor the context in which community engagement takes place</li>
	<li>
		Reflect on your experience with the scholarship and practice of community engagement</li>
	<li>
		Document processes, outcomes, and evidence related to community engagement methodically</li>
	<li>
		Solicit critical feedback from community partners and mentors</li>
	<li>
		Prepare and present your community-engaged scholarship for peer review</li>
	<li>
		Generate new insights through critical and reflective writing</li>
	<li>
		Practice writing and speaking about your community-engaged scholarship or practice</li>
	<li>
		Practice talking about yourself as an engaged scholar or practitioner</li>
</ul>

<p>
	As the culminating experience in the MSU Graduate Certification in Community Engagement, the written portfolio and presentation are equivalent to the final exam for the program. <strong>Together, they are your opportunity to synthesize and document what you have learned from the readings, seminar/workshops, mentored community engagement experience, and through reflection on your experiences with your community partners and mentor</strong>.
</p>

<hr />

<h2>
	Distinctions Between the <br /> Written Portfolio and the Presentation
</h2>

<p>
	In the <strong>written portfolio</strong>, you <strong>demonstrate your understanding</strong> of <a href="required-core-competencies"><strong>15 of the program’s 20 core competencies</strong></a>, with the goal of being as <strong>comprehensive</strong> and <strong>thorough</strong> as possible.
</p>

<p>
	In contrast, for your <strong>portfolio</strong> <strong>presentation</strong>, you <strong>summarize your experiences and reflections</strong>, with an <strong>in-depth focus</strong> on <a href="competencies"><strong>two</strong> <strong>core competencies</strong></a> of your choice.
</p>

<br />

<p>
	<a class="btn btn-theme btn-theme-accent" href="written-portfolio-guidelines">
    Written Portfolio Guidelines
  </a>
</p>

<p>
  <a class="btn btn-theme btn-theme-accent" href="portfolio-presentation-guidelines">
    Portfolio Presentation Guidelines
  </a>
</p>

<p>
  <a class="btn btn-theme btn-theme-accent document" title="PDF: 161.7 KB" <?php echo $pdf; ?> href="upload/2019/Portfolio-Cover-Page-2019-2020.pdf" target="_blank">
    Portfolio Cover Page
  </a>
</p>


<br />
<br />
<?php include("Views/Shared/Partials/requirements-nav.php") ?>
