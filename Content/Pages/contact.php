<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?></h1>

<div class="row">
  <p class="col-md-8">
    If you would like to discuss the Graduate Certification in Community Engagement requirements and/or the application process, please contact:
  </p>

  <dl class="col-md-8">
    <dd> <strong>Diane Doberneck</strong> </dd>
    <dd> Coordinator, MSU Graduate Certification in Community Engagement </dd>
    <dd> Associate Director for Faculty and Professional Development </dd>
    <dd> Office for Public Engagement and Scholarship </dd>
    <dd> University Outreach and Engagement </dd>
    <dd> E-mail: <a href="mailto:connordm@msu.edu">connordm@msu.edu</a> </dd>
    <dd> Phone: (517) 353-8977 </dd>
  </dl>
</div>

<hr class="divider" />

<h2 class="h5 fc-body">
  For additional information:
</h2>


<div class="row">
  <dl class="col-md-5">
    <dd>
      <strong>
        <a href="http://outreach.msu.edu" class="external" target="_blank" <?php echo $external ?>>
          University Outreach and Engagement
        </a>
      </strong>
    </dd>
    <dd> Kellogg Hotel and Conference Center </dd>
    <dd> 219 S. Harrison Road, Room 93 </dd>
    <dd> East Lansing, MI 48824 </dd>
    <dd> Phone: (517) 353-8977 </dd>
    <dd> Fax: (517) 432-9541 </dd>
  </dl>

  <dl class="col-md-5">
    <dd>
      <strong>
        <a href="http://grad.msu.edu" class="external" target="_blank" <?php echo $external ?>>
          The Graduate School
        </a>
      </strong>
    </dd>
    <dd> Chittenden Hall </dd>
    <dd> 466 West Circle Drive </dd>
    <dd> East Lansing, MI 48824 </dd>
    <dd> Phone: (517) 353-3220 </dd>
  </dl>
</div>
