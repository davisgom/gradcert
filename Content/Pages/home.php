<section class="container intro">

  <div class="row flex-column">
    <h2 class="col-9 col-sm-6">
      Developing the skills and <br /> competencies needed for exemplary <br class="d-sm-none d-md-block" /> university-community engagement
    </h2>

    <div class="col-8 col-sm-6 col-lg-5 col-xl-4 alert alert-dismissible fade show p-0">

      <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="z-index: 100; mix-blend-mode: multiply; opacity: 0.3;">
        <span aria-hidden="true">&times;</span>
      </button>

      <p class="deadline">
        <strong>Applications Due:</strong><br />
        <?php echo $application_deadline; ?>, 5:00<small>pm</small>
      </p>
      <p class="ml-10"><a href="application">View application details</a></p>
    </div>
  </div>

  <div class="people-bg">
    <div class="people">
      <div class="character character-box <?php echo $people[$random_person[0]];?>">
        <div class="person"></div>
        <div class="circle"></div>
      </div>

      <div class="character character-box d-none d-sm-block <?php echo $people[$random_person[1]];?>">
        <div class="person"></div>
        <div class="circle"></div>
      </div>

      <div class="character character-box d-none d-lg-block <?php echo $people[$random_person[2]];?>">
        <div class="person"></div>
        <div class="circle"></div>
      </div>

      <div class="character character-box d-none d-lg-block <?php echo $people[$random_person[3]];?>">
        <div class="person"></div>
        <div class="circle"></div>
      </div>
    </div>
  </div>

</section>


<?php include "Views/Shared/Partials/info-session.php" ?>


<hr />


<section class="container mt-5">
  <div class="row">
    <div class="col-11 mx-auto">
      <p>
        Michigan State University's Graduate Certification in Community Engagement prepares students for academic and professional careers that integrate scholarship with community engagement.
      </p>

      <p>
        The Certification is designed to help graduate and professional students develop systemic, respectful, and scholarly approaches to their community-engaged work. With approval from their Guidance Committee chairperson and University Outreach and Engagement, students tailor their program of study to strengthen their scholarly and practical skills in community-engaged research and creative activities, community-engaged teaching and learning, community-engaged service and practice, and/or community-engaged commercialization activities.
      </p>

      <p>
        Students who fulfill all requirements receive a letter of congratulations from the Associate Provost for University Outreach and Engagement, an official notation on their academic transcript, and a certificate of completion from MSU’s Office of the Registrar.
      </p>
    </div>
  </div>
</section>
