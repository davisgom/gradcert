<div class="pt-40 pb-30 pb-md-30">
  <?php include("Views/Shared/Partials/page-banner.php"); ?>
</div>

<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?></h1>

<p>
  Michigan State University's Graduate Certification in Community Engagement is an initiative of <a href="http://engage.msu.edu/" <?php echo $external ?> target="_blank">University Outreach and Engagement</a> and <a href="http://grad.msu.edu/" <?php echo $external ?> target="_blank">The Graduate School</a>. The Certification is designed to help graduate and professional students develop systemic, respectful, and scholarly approaches to their community-engaged work. With approval from their Guidance Committee Chairperson and University Outreach and Engagement, students tailor their program of study to strengthen their scholarly and practical skills in community-engaged research and creative activities, community-engaged teaching and learning, community-engaged service and practice, and/or community-engaged commercialized activities.
</p>

<p>
  To complete the Certification, students must show mastery of <a href="competencies">core engagement competencies</a>, complete a 60-hour <a href="community-experience">mentored community engagement experience</a>, and write and present an <a href="engagement-portfolio">engagement portfolio</a>.
</p>

<p>
  Students who fulfill all requirements receive a letter of congratulations from the Associate Provost for University Outreach and Engagement, an official notation on their academic transcript, and a certificate of completion from MSU’s Office of the Registrar.
</p>

<hr class="divider">

<h2>Goals of the Certification <br /> in Community Engagement</h2>

<p>
  Modeled after MSU's Certification in College Teaching, this Certification is designed to help graduate and professional students:
</p>

<ul>
	<li>Prepare for a career as a community-engaged scholar or practitioner</li>
	<li>Learn about scholarly approaches to community engagement</li>
	<li>Gain skills for collaborating effectively and respectfully with community partners</li>
	<li>Share disciplinary knowledge and experiences with other graduate students, faculty, and staff</li>
	<li>Network with other engaged scholars and practitioners—on campus and nationally</li>
</ul>

<br />
<br />

<h3>Benefits of the Program</h3>

<p>
	<strong>For graduate and professional students:</strong> Completing the Graduate Certification in Community Engagement signifies that they:
</p>

<ul>
	<li>value outreach and engagement as a scholarly activity</li>
	<li>have mastered a set of core engagement competencies</li>
	<li>have put their scholarly and practical skills into practice in a community setting</li>
	<li>have critically reflected on their community engagement experiences</li>
</ul>

<br />

<p>
  <strong>For prospective employers:</strong> The Certification signifies that students have acquired the mindsets and skills for successfully engaging the community in ways that are collaborative, respectful, and scholarly.
</p>

<hr class="divider">

<h2>About Us</h3>

<p>
	<a href="http://outreach.msu.edu" <?php echo $external; ?> target="_blank">University Outreach and Engagement (UOE)</a> fosters MSU's <a href="http://president.msu.edu/mission" <?php echo $external; ?> target="_blank">land-grant mission</a> by connecting university knowledge with community knowledge in mutually beneficial ways. UOE provides resources to assist academic departments, centers and institutes, and MSU Extension on priority issues of concern to society by encouraging, supporting, and collaborating with MSU faculty and academic staff to generate, apply, transmit and preserve knowledge.
</p>

<p>
	MSU’s University Outreach and Engagement advocates for a model of outreach and engagement that fosters a reciprocal and mutually beneficial relationship between the University and the public. UOE promotes the scholarly aspect of community engagement by emphasizing both the scholarly foundations that inform community engagement and the academic and public products that are generated as a result of community-engaged work.
</p>

<p>
	The MSU Graduate Certification in Community Engagement is based on nationally recognized core engagement competencies and decades of practical experience working with community partners. UOE faculty and staff have developed the Certification to strengthen and enhance the multi-disciplinary skills needed for exemplary community engagement.
</p>

<p>
	We hope that the Graduate Certification in Community Engagement will serve as a model for the development of the next generation of engaged scholars and practitioners across colleges, departments, and disciplines at Michigan State University and beyond. To discuss how to establish a similar program for graduate students at your campus, please contact the program coordinator, <a href="mailto:connordm@msu.edu">Diane Doberneck</a>.
</p>

<hr class="divider">

<ul>
	<li>
		<a href="https://quod.lib.umich.edu/cgi/t/text/text-idx?cc=mjcsloa;c=mjcsl;c=mjcsloa;idno=3239521.0024.111;view=text;rgn=main;xc=1;g=mjcslg" <?php echo $external; ?> target="_blank">Community Engagement Competencies for Graduate and Professional Students: Michigan State University’s Approach to Professional Development</a></li>
</ul>
