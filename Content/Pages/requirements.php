<div class="pt-40 pb-30 pb-md-30">
  <?php include("Views/Shared/Partials/page-banner.php"); ?>
</div>

<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?></h1>

<p>
	To earn the Graduate Certification in Community Engagement, <br class="d-none d-md-block" /> graduate and professional students must complete three requirements:
</p>

<ol class="requirements-list mt-lg-40">
	<li class="my-20 my-md-0">
    <h2 class="h4">
      <a href="competencies">Core Engagement Competencies</a>
    </h2>

    <p> A series of seminars covering various engagement topics. </p>

    <a href="competencies" class="btn btn-theme btn-theme-primary">
      Read more
      <span class="sr-only">about the core engagement competencies requirement</span>
    </a>
  </li>

  <li class="my-20 my-md-0">
    <h2 class="h4">
      <a href="community-experience" class="fc-secondary">Community <br /> Experience</a>
    </h2>

    <p> A mentored community engagement experience. </p>

    <a href="community-experience" class="btn btn-theme btn-theme-secondary">
      Read more
      <span class="sr-only">about the community engagement experience requirement</span>
    </a>
  </li>

	<li class="my-20 my-md-0">
    <h2 class="h4">
      <a href="engagement-portfolio" class="fc-accent">Engagement <br /> Portfolio</a>
    </h2>

    <p>	A written engagement portfolio and presentation. </p>

    <a href="engagement-portfolio" class="btn btn-theme btn-theme-accent">
      Read more
      <span class="sr-only">about the engagement portfolio requirement</span>
    </a>
  </li>
</ol>

<hr class="divider" />

<h2>
	Flexibility
</h2>

<p>
	The Graduate Certification in Community Engagement is designed to reinforce coherent integration of community-engaged scholarship into the student's academic program. Each aspect of the Certification offers maximum flexibility:
</p>

<ul>
	<li>
		<strong>Timing:</strong> The program is organized so that students may complete the Graduate Certification in one year or over multiple years, depending on how the Certification's requirements can best be coordinated with their degree program plans.
	</li>

	<li>
		<strong>Competencies:</strong> The core engagement competency requirement may be fulfilled through seminars offered by University Outreach and Engagement, through other academic experiences or coursework, or through some combination of the two options. On a case by case basis, the program coordinator strategizes with students about the best option for fulfilling the seminar requirement, and as needed, reviews and approves alternatives to the seminars.
	</li>

	<li>
		<strong>Experience:</strong> The mentored community engagement experience requirement may be fulfilled by community work that the student is already undertaking (or planning to undertake). The majority of students in the Graduate Certification in Community Engagement use experiences associated with their program of study for this requirement, including practicums, internships, thesis or dissertation research, graduate assistantships, or teaching experiences—<span class="red">as long as it meets the definition of community-engaged scholarship</span>.
	</li>

	<li>
		<strong>Written portfolio:</strong> While there are guidelines for the engagement portfolio, students are encouraged to reflect upon and share their views on community-engaged scholarship in a manner consistent with their disciplinary and professional norms. As a result, engagement portfolios vary in length, format, and voice.
	</li>

	<li>
		<strong>Portfolio presentation:</strong> Engagement portfolios may be presented at regularly scheduled times at the end of Fall or Spring semesters, during the Summer semester on an ad-hoc basis, or via web technologies from a non-campus location. Flexible timing of engagement portfolio presentations reinforces the primary importance of students completing their program degree requirements first and their Graduate Certification in Community Engagement requirements second.
	</li>

	<li>
		<strong>Evolving plans:</strong> Throughout this professional development experience, students are encouraged to meet with the program coordinator to discuss their evolving plans and opportunities for completing the requirements.
	</li>
</ul>

<hr class="divider" />

<ul>
	<li>
		<a href="key-concepts-about-community-engaged-scholarship-at-msu">
			Key Concepts in Community-Engaged Scholarship
		</a>
	</li>
</ul>
