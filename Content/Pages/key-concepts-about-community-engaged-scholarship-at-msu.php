<h1 class="page-title">
  Key Concepts about <br /> Community-Engaged Scholarship at MSU
</h1>

<h2>
  For Faculty, Academic Staff, and Graduate Students, <span class="small d-block d-lg-inline">updated Jan. 2018</span>
</h2>

<hr class="divider">

<h3>
	MSU's Definition
</h3>

<p>
	(Provost's Committee on University Outreach, 1993)
</p>

<p>
	At Michigan State University, <strong>community-engaged scholarship</strong> is defined as "a form of scholarship that cuts across teaching, research [and creative activities], and service. It involves generating, transmitting, and applying knowledge for the direct benefit of external audiences in ways that are consistent with university and unit missions."
</p>

<p>
	That means, Community-Engaged Scholarship is <strong>not</strong>
</p>

<ul>
	<li>
		Serving on a departmental committee.
  </li>

	<li>
		Serving on a university-wide committee.
  </li>

	<li>
		Serving on a disciplinary committee.
  </li>

	<li>
		Volunteering not related to your discipline or not associated with community partnerships in your academic field.
  </li>

	<li>
		Conducting outside work for pay, with no connection or benefit to your departmental/unit missions.
  </li>
</ul>

<hr class="divider">

<h3>
	All scholarship, including community—engaged scholarship:
</h3>

<p>
	(Diamond, 2002; Jordan, 2007)
</p>

<ul>
	<li>
		Requires high level of disciplinary (or interdisciplinary) expertise.
  </li>

	<li>
		Uses an appropriate methodology.
  </li>

	<li>
		Is appropriately and effectively documented and disseminated to (academic and community) audiences, with reflective critique about significance, processes, and lessons learned.
  </li>

  <li>
		Has significance beyond the individual context (breaks new ground, innovative, can be replicated or elaborated).
  </li>

  <li>
		Is judged to be significant and meritorious (product, process, and/or results) by panel of peers.
  </li>

	<li>
		Demonstrates consistently ethical practice, adhering to codes of conduct in research, teaching, and the discipline.
  </li>
</ul>

<hr class="divider" />

<div class="row">

<div class="col-md-4">
  <p>
  	By <strong>community</strong>, we mean groups of people who share commonalities, incl. (Fraser, 2005; Ife, 1995; Marsh, 1996, Mattessich &amp; Monsey, 1997)
  </p>

  <p>
    <ul>
    	<li>Geography.</li>
    	<li>Identity.</li>
    	<li>Affiliation or Interest.</li>
    	<li>Circumstance.</li>
    	<li>Profession or Practice.</li>
    	<li>Faith.</li>
    	<li>Family/Kin.</li>
    </ul>
  </p>
</div>

<div class="col-md-4">
<p>
	By <strong>engagement</strong>, we mean the work can be described as (Fitzgerald, Smith, Book, &amp; Rodin, 2005)
</p>

<p>
  <ul>
  	<li>Scholarly.</li>
  	<li>Systemic.</li>
  	<li>Collaborative.</li>
  	<li>Transformative.</li>
  	<li>Asset Based.</li>
  	<li>Mutually Beneficial.</li>
  	<li>Capacity Building.</li>
  	<li>For the Public Good.</li>
  </ul>
</p>
</div>

<div class="col-md-4">
<p>
	By <strong>scholarly</strong>, we mean it is based on existing scholarship, best practices, understandings <strong>and</strong> generative of new understandings and scholarly products for academic and public audiences (Ellison &amp; Eatman, 2008).
</p>
</div>
</div>

<hr class="divider">

<div class="pt-80 pb-40">
  <?php include("Views/Shared/Partials/page-banner.php"); ?>
</div>

<h3 id="scholarship-figure">
	Community-Engaged Scholarship Figure <span class="small d-block d-lg-inline">(Doberneck et al, 2017)</span>
</h3>

<div class="lead">
<p>
  In Collaboration with Community Partners
  <span class="small">(including local, indigenous, and/or practitioner knowledge)</span>
</p>
</div>

<!-- #SCHOLARSHIP FIGURE -->
<div class="scholarship-figure">
  <div class="col col-md-3">
  		<p>
  			Foundational scholarship informs your understanding and guides...
      </p>
  </div>

  <div class="col col-md-3">
  		<p>
  			...your engagement experiences with your community partners, which, then...
      </p>
  </div>

  <div class="col col-md-3">
  		<p>
  			...generate new scholarship and practice for both...
      </p>
  </div>

  <div class="col col-md-3">
  		<p>
		    public <br class="d-none d-md-block" />
        audiences

        <br />
        <br />

        academic <br class="d-none d-md-block" />
        audiences
      </p>
  </div>
</div>

<br class="clear">


	<hr>

<h3>
  Common Types of Community-Engaged <br /> Scholarship Reported by Faculty
</h3>

<p>
	(Doberneck, Glass, &amp; Schweitzer, 2010)—Updated and Revised, August 2015
</p>

<table class="key-concepts table table-bordered table-responsive">
	<caption class="sr-only">
		Common Types of Community-Engaged Scholarship Reported by Faculty
  </caption>

  <thead>
		<tr>
			<th colspan="4" id="header" scope="col">
				Community-Engaged Scholarship Conducted in Response to Communities or in the Context of Community Partnerships
      </th>
		</tr>
		<tr>
			<th id="header1" scope="col">Community-Engaged Research and Creative Activities</th>
			<th id="header2" scope="col">Community-Engaged Teaching and Learning</th>
			<th id="header3" scope="col">Community-Engaged Service and Practice</th>
			<th id="header4" scope="col">Community-Engaged Commercialized Activities</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td headers="header1 header">
				<p>
					<em>Engaged research and creative activities are associated with the discovery of new knowledge, the development of new insights, and the creation of new artistic or literary performances and expressions—in collaboration with community partners.</em></p>
			</td>
			<td headers="header2 header">
				<p>
					<em>Engaged teaching is organized around sharing knowledge with various audiences through either formal or informal arrangements. Types of engaged teaching vary by relationship among the teacher, the learner, and the learning context. Engaged teaching may be for-credit or not-for-credit, guided by a teacher or self-directed.</em></p>
			</td>
			<td headers="header3 header">
				<p>
					<em>Engaged service is associated with the use of university expertise to address specific issues (ad hoc or long—term) identified by individuals, organizations, or communities. This type of engagement is not primarily driven by a research question, though a research question may be of secondary interest in the activity.</em></p>
			</td>
			<td headers="header4 header">
				<p>
					<em>Commercialized activities are associated with a variety of projects in which universitygenerated knowledge is translated into practical or commercial applications for the benefit of individuals, organizations, or communities.</em></p>
			</td>
		</tr>
		<tr>
			<td headers="header1 header">
				<h2>
					Community-Engaged Research</h2>
				<ul>
					<li>
						Community-based, participatory research</li>
					<li>
						Applied research</li>
					<li>
						Contractual research (funded by government, non—governmental organizations, or businesses)</li>
					<li>
						Demonstration projects</li>
					<li>
						Needs and assets assessments</li>
					<li>
						Program evaluations</li>
				</ul>
				<h2>
					Community-Engaged Creative Activity</h2>
				<ul>
					<li>
						Collaboratively created, produced, or performed
						<ol>
							<li>
								Film</li>
							<li>
								Theater</li>
							<li>
								Music</li>
							<li>
								Performance</li>
							<li>
								Sculpture</li>
							<li>
								Writing</li>
							<li>
								Spoken word performance</li>
							<li>
								Multi—media</li>
							<li>
								Exhibitions</li>
						</ol>
					</li>
				</ul>
			</td>
			<td headers="header2 header">
				<h2>
					Formal <span class="small d-block">(For-Credit)</span></h2>
				<ul>
					<li>
						Service-learning</li>
					<li>
						Community-engaged research as part of university classes</li>
					<li>
						Study abroad programs with community engagement components</li>
					<li>
						Online and off—campus education</li>
				</ul>
				<h2>
					Non-formal <span class="small d-block">(Not-for-Credit)</span></h2>
				<ul>
					<li>
						Pre-college programs for youth in K-12</li>
					<li>
						Occupational short course, certificate, and licensure programs</li>
					<li>
						Conferences, seminars, not-for-credit classes, and workshops</li>
					<li>
						Educational enrichment programs for the public and alumni</li>
				</ul>
				<h2>
					Informal <span class="small d-block">(Not-for-Credit)</span></h2>
				<ul>
					<li>
						Media interviews or "translational" writing for general public audiences</li>
					<li>
						Materials to enhance public understanding</li>
					<li>
						Self-directed, managed learning environments, such as museums, libraries, gardens</li>
				</ul>
			</td>
			<td headers="headers3 header">
				<ul>
					<li>
						Technical assistance</li>
					<li>
						Consulting</li>
					<li>
						Policy analysis</li>
					<li>
						Expert testimony</li>
					<li>
						Legal advice</li>
					<li>
						Clinical practice</li>
					<li>
						Diagnostic services</li>
					<li>
						Human and animal patient care</li>
					<li>
						Advisory boards and other disciplinary-related service to community organizations</li>
				</ul>
			</td>
			<td headers="headers4 header">
				<ul>
					<li>
						Copyrights</li>
					<li>
						Patents</li>
					<li>
						Licenses for commercial use</li>
					<li>
						Innovation and entrepreneurship activities</li>
					<li>
						University-managed or supported business ventures, such as business parks or incubators</li>
					<li>
						New business ventures and start-ups</li>
					<li>
						Inventions</li>
					<li>
						Social entrepreneurship</li>
				</ul>
			</td>
		</tr>
	</tbody>
</table>
