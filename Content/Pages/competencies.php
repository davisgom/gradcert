<?php include("Views/Shared/Partials/requirements-nav.php") ?>

<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?></h1>

<h2>
	Core Engagement Competencies <span class="small d-block d-lg-inline">(First Requirement)</span>
</h2>

<p>
	The first requirement for the Graduate Certification in Community Engagement is the core engagement competencies. For the 2020-2021 academic year, the core engagement competencies include topics:</p>
<ul>
	<li>
		Foundations of community-engaged scholarship</li>
	<li>
		Variations in community-engaged scholarship</li>
	<li>
		Initiating community partnerships</li>
	<li>
		Sustaining community partnerships</li>
	<li>
		Techniques for community collaboration</li>
	<li>
		Critical thinking and critical reflections</li>
	<li>
		Community-engaged research and creative activities</li>
	<li>
		Community-engaged teaching and learning</li>
	<li>
		Community-engaged service and practice</li>
	<li>
		Asset based community engagement</li>
	<li>
		Capacity building for sustained change</li>
	<li>
		Systemic approaches to community change</li>
	<li>
		Evaluation of community partnerships</li>
	<li>
		Quality, excellence, and rigor in peer review</li>
	<li>
		Communicating with public audiences</li>
	<li>
		Communicating with academic audiences</li>
	<li>
		Documenting your engagement accomplishments</li>
	<li>
		Successful community engagement careers</li>
	<li>
		Working with diverse communities</li>
	<li>
		Ethics and community-engaged scholarship</li>
</ul>
<p>
	Students usually fulfill this requirement by attending <a href="#seminar">seminars</a> coordinated by University Outreach and Engagement. The series is offered on an annual basis, in person, Friday afternoons from 1:30-3:30 PM during Fall and Spring semesters.
</p>

<p>
	Students who have completed coursework that addresses a particular core engagement competency may ask to have the syllabi, reading lists, and assignments evaluated as potential alternatives to the required seminars. Contact the program coordinator to request such a review.
</p>

<p>
	In rare instances, students may request a make-up session to review materials from the core engagement seminars.
</p>

<div class="pt-80 pb-40">
  <?php include("Views/Shared/Partials/page-banner.php"); ?>
</div>


<p class="sr-only">
	<a href="#spring2021" title="Skip to view Spring 2021 Seminar Information">
    Skip to view Spring 2021 Seminar Information
  </a>
</p>


<h2 id="seminar">
	Seminar Schedule 2020-2021<br>
	<span class="small">Graduate Certification in Community Engagement</span>
</h2>

<p>
	<em class="small">Last Updated: June 8, 2020</em>
</p>

<br />

<h3>Fall Semester 2020 Seminars</h3>

<table class="seminar fall" id="fall2020">
	<caption class="sr-only">
		This table lists Seminars and follow-up sessions available for the Graduate Certificate in Community Engagement for Fall 2020.
  </caption>

  <thead>
		<tr>
			<th class="header" id="fallDateTime" scope="row">
				<strong>Date/Time</strong>
      </th>
      <th class="header" id="fallSeminarTopic" scope="row">
				<strong>Seminar Topic</strong>
      </th>
		</tr>
	</thead>

  <tbody>
		<tr>
			<td class="datetime" headers="fallDateTime" id="time1" scope="row">
				<span class="date">Thurs., Sept. 3</span>
        <br>(optional)<br>
				<span class="time">1:30-3:30 pm</span><br>
				<span class="room">62 Kellogg &amp; Zoom</span>
      </td>
			<td headers="fallSeminarTopic time1">
				<strong>
          Information Session about Graduate Certification in Community Engagement
        </strong>
      </td>
		</tr>

		<tr>
			<td class="datetime" headers="fallDateTime" id="time2" scope="row">
				<span class="date">Fri., Sept. 11</span><br>
				<span class="time">5:00 pm</span>
      </td>
			<td headers="fallSeminarTopic time2">
				<strong>
          <a href="/application">On-Line Applications &amp; Nominations Deadline</a>
        </strong>
      </td>
		</tr>
		<tr>
			<td class="datetime" headers="fallDateTime" id="time3" scope="row">
				<span class="date">Fri., Sept. 18</span><br>
				(double session)<br>
				<span class="time">1:30-3:00 pm (1<sup>st</sup>)<br>
				3:15-5:00 pm (2<sup>nd</sup>)</span><br>
				<!-- <span class="room">138 Brody</span> -->
      </td>
			<td headers="fallSeminarTopic time3">
				<p>
					<strong>Seminar 1: History and Foundations of Community-Engaged Scholarship</strong></p>
				<!-- <p> --><!-- <em>Burton Bargerstock, Executive Director, Office for Public Engagement and Scholarship (OPES), Michigan State University</em></p> -->
				<p>
					<strong>Seminar 2: Variations in Community-Engaged Scholarship</strong></p>
				<!-- <p> --><!-- <em>Diane Doberneck, Director for Faculty and Professional Development, OPES, Michigan State University</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="fallDateTime" id="time4" scope="row">
				<span class="date">Fri., Sept. 25</span><br>
				(out of order)<br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="fallSeminarTopic time4">
				<p>
					<strong>Seminar 3: Initiating and Sustaining Community Partnerships</strong></p>
				<!-- <p> --><!-- <em>Renee Brown, Director; and Michelle Snitgen, Academic Specialist, Center for Community Engaged Learning, Michigan State University</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="fallDateTime" id="time5" scope="row">
				<span class="date">Fri., Oct. 9</span><br>
				(extra long)<br>
				<span class="time">1:30-4:00 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="fallSeminarTopic time5">
				<p>
					<strong>Seminar 4: Engaging with Diverse Communities</strong></p>
				<!-- <p> --><!-- <em>Nicole Springer, Director, Campus Compact for Michigan</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="fallDateTime" id="time6" scope="row">
				<span class="date">Fri. Oct. 23</span><br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="fallSeminarTopic time6">
				<p>
					<strong>Seminar 5: Techniques for Community Collaboration</strong></p>
				<!-- <p> --><!-- <em>Diane Doberneck and others for lightning talks</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="fallDateTime" id="time9" scope="row">
				<span class="date">Fri., Nov. 6</span><br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="fallSeminarTopic time9">
				<p>
					<strong>Seminar 6: Community-Engaged Research and Creative Activities</strong></p>
				<!-- <p> --><!-- <em>Speaker: </em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="fallDateTime" id="time11" scope="row">
				<span class="date">Fri., Nov. 20</span><br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="fallSeminarTopic time11">
				<p>
					<strong>Seminar 7: Community-Engaged Teaching and Learning</strong></p>
				<!-- <p> --><!-- <em>Renee Brown, Director; and Michelle Snitgen, Academic Specialist, Center for Community Engaged Learning, Michigan State University</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="fallDateTime" id="time12" scope="row">
				<span class="date">Fri., Dec. 4</span><br>
				(out of sequence)<br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="fallSeminarTopic time12">
				<p>
					<strong>Seminar 8: Community-Engaged Service and Practice</strong></p>
				<!-- <p> --><!-- <em>Speaker: </em></p> --></td>
		</tr>
	</tbody>
</table>





<h3 class="mt-80">Spring Semester 2021 Seminars</h3>

<table class="seminar spring" id="spring2021">
	<caption class="sr-only">
		This table lists Seminars and follow-up sessions available for the Graduate Certificate in Community Engagement for Spring 2021.
  </caption>

  <thead>
		<tr>
			<th class="header" id="springDateTime" scope="row">
				<strong>Date/Time</strong>
      </th>
      <th class="header" id="springSeminarTopic" scope="row">
				<strong>Seminar Topic</strong>
      </th>
		</tr>
	</thead>

  <tbody>
		<tr>
			<td class="datetime" headers="springDateTime" id="time13" scope="row">
				<span class="date">Fri., Jan. 15</span><br>
				(double session)<br>
				<span class="time">1:30-3:00 pm (1<sup>st</sup>)<br>
				3:15-5:00 pm (2<sup>nd</sup>)</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="spring time13">
				<p>
					<strong>Seminar 9: Documenting and Communicating Engagement Accomplishments</strong></p>
				<!-- <p> --><!-- <em>Miles McNall, Director for Community-Engaged Research, OPES, Michigan State University</em></p> -->
				<p>
					<strong>Seminar 10: Community Engagement Across the Career Span</strong></p>
				<!-- <p> --><!-- <em>Diane Doberneck, Director for Faculty and Professional Development, OPES, Michigan State University</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="springDateTime" id="time14" scope="row">
				<span class="date">Fri., Jan. 29</span><br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="springSeminarTopic time14">
				<p>
					<strong>Seminar 11: Coalition Approaches to Community Engagement</strong></p>
				<!-- <p> --><!-- <em>Kas Anderson-Carpenter, Department of Psychology, Michigan State University</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="springDateTime" id="time15" scope="row">
				<span class="date">Fri., Feb. 12</span><br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="springSeminarTopic time15">
				<p>
					<strong>Seminar 12: Cross-Cultural Dimensions of Community Partnerships</strong></p>
				<!-- <p> --><!-- <em>Speaker: </em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="springDateTime" id="time17" scope="row">
				<span class="date">Fri., Feb. 26</span><br>
				(extra long)<br>
				<span class="time">1:30-4:00 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="springSeminarTopic time17">
				<p>
					<strong>Seminar 13: Asset-based and Capacity-Building Approaches</strong></p>
				<!-- <p> --><!-- <em>Speaker: </em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="springDateTime" id="time18" scope="row">
				<span class="date">Fri., Mar. 19</span><br>
				(out of sequence)<br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="springSeminarTopic time18">
				<p>
					<strong>Seminar 14: Evaluation of Community Partnerships</strong></p>
				<!-- <p> --><!-- <em>Miles McNall, Director for Community Engaged Research, OPES, Michigan State University</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="springDateTime" id="time20" scope="row">
				<span class="date">Fri., Apr. 2</span><br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="springSeminarTopic time20">
				<p>
					<strong>Seminar 15: Peer Review of Community-Engaged Scholarship</strong></p>
				<!-- <p> --><!-- <em>Miles McNall and Diane Doberneck</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="springDateTime" id="time21" scope="row">
				<span class="date">Fri., Apr. 16</span><br>
				(extra long)<br>
				<span class="time">1:30-4:00 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="springSeminarTopic time21">
				<p>
					<strong>Seminar 16: Communicating with Public Audiences</strong></p>
				<!-- <p> --><!-- <em>Danielle DeVoss, Department of Writing, Rhetoric, and American Cultures, Michigan State University</em></p> --></td>
		</tr>
		<tr>
			<td class="datetime" headers="springDateTime" id="time22" scope="row">
				<span class="date">Fri., Apr. 30</span><br>
				<span class="time">1:30-3:30 pm</span><br>
				<!-- <span class="room">138 Brody</span> --></td>
			<td headers="springSeminarTopic time22">
				<p>
					<strong>Seminar 17: Communicating with Academic Audiences</strong></p>
				<!-- <p> --><!-- <em>Diane Doberneck, Director for Faculty and Professional Development, OPES, Michigan State University</em></p> --></td>
		</tr>
	</tbody>
</table>

<div class="mt-80">
	<?php include("Views/Shared/Partials/requirements-nav.php") ?>
</div>
