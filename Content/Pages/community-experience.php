<?php include("Views/Shared/Partials/requirements-nav.php") ?>

<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?></h1>

<h2>
	Mentored Community Engagement Experience <span class="small d-block">(Second Requirement)</span>
</h2>

<p>
	The second requirement for the Graduate Certification in Community Engagement is the mentored community engagement experience. The mentored community engagement experience is an opportunity to collaborate with a community partner and a faculty mentor on a project in a real-world, community setting. The goal is to implement core engagement concepts introduced during the core competency seminars and to gain practical experience collaborating with community partners.
</p>

<p>
	Mentored community engagement experiences can be any form of community-engaged scholarship, including community-engaged research and creative activities, community-engaged teaching and learning, community-engaged service and practice, and/or community-engaged commercialized activities.
</p>

<p>
	In order for a potential experience to be approved for the Graduate Certification in Community Engagement requirement, it must:
</p>

<ul>
	<li>
		Be approved in advance by the program coordinator
  </li>

	<li>
		Meet the MSU's definition of community-engaged scholarship (especially the scholarly dimensions
    )</li>

	<li>
		Involve community partners from beyond campus
  </li>

	<li>
		Be collaboratively undertaken with community partner(s) and a mentor
  </li>

	<li>
		Involve significant, direct interaction between you and your community partner
  </li>

	<li>
		Include reflection on communication, collaboration, and partnering skills with a mentor
  </li>

	<li>
		Include critical feedback from the community partner about your collaborative work together
  </li>

	<li>
		Be 60 hours at the minimum
  </li>
</ul>

<div class="pt-80 pb-40">
  <?php include("Views/Shared/Partials/page-banner.php"); ?>
</div>


<p>
	Students are expected to keep an activity log of days, hours, and tasks they complete as part of their mentored community engagement experience. This log will become part of their written engagement portfolio.</p>
<p>
	In addition, students are expected to critically reflect on their experience with their community partners and gather feedback from them. This critical feedback may take different forms depending on the circumstances, and should also be included in the written engagement portfolio. The program coordinator will furnish past examples of activity logs and community partner feedback to interested students.
</p>

<br />

<p>
  <a class="btn btn-theme btn-theme-secondary" href="mentored-community-engagement-experience-guidelines">
    Mentored Community Engagement Experience Guidelines
  </a>
</p>

<p>
  <a class="btn btn-theme btn-theme-secondary document" title="PDF: 114.9 KB" <?php echo $pdf; ?> href="/upload/2019/Community Partner Feedback Guide, 2019-2020.pdf" target="_blank">
    Community Partner Feedback Form
  </a>
</p>

<br />
<br />
<?php include("Views/Shared/Partials/requirements-nav.php") ?>
