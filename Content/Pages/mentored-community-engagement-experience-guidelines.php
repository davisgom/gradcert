<div class="mb-30">
	<a href="community-experience">
		<i class="fa fa-arrow-circle-left"></i> Back to Community Experience
	</a>
</div>

<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?>, 2019-2020</h1>

<h2>
	Purpose of Mentored Community Engagement Experience
</h2>

<p>
	The mentored community engagement experience is second requirement for the MSU Graduate Certification in Community Engagement. It is an opportunity for you to collaborate with one (or more) community partners and a mentor on a scholarly outreach or engagement project in a real-world, community setting. The goal is to implement key ideas and practices introduced during the program’s seminars/workshops, when possible, and to gain practical experience with trust-building, communications, relationship building, collaboration, and reflection with your community partner(s).
</p>

<p>
	Mentored community engagement experiences vary by discipline, college, and personal interests. They may be any form of community engaged scholarship, including community engaged research and creative activities, community engaged teaching and learning, community engaged service, community engaged commercialization activities, and/or any combination. They may involve any type of community or community partners. They may occur at any scale—from simple partnership structures to complex, networked structures that span regions, states or countries.
</p>

<p>
	For a majority of learners, the mentored community engagement experience is associated with their graduate degree program and may be a practicum, internship, thesis or dissertation research, graduate assistantship work, teaching responsibilities, or work experience. The mentored community engagement experience does <strong>not</strong> have to be a new or additional community engagement activity unless you would like it to be.
</p>

<h2>
	Relationship to the <br class="d-none d-md-block" /> Graduate Certification’s Other Requirements
</h2>

<p>
	While some learners complete the seminars/workshops and the mentored community engagement experience simultaneously, it is recommended that your take the seminars/workshops before completing your mentored community engagement experience if possible. A description of your mentored community engagement experience and your critical reflections on it will form a major part of your written portfolio and presentation.
</p>

<h2>
	Requirements
</h2>

<p>
	To meet the requirements of the Graduate Certification in Community Engagement, your mentored community engagement experience must:
</p>

<ul>
	<li>
		Be approved in advance by the program coordinator
  </li>

	<li>
		Meet MSU’s definition of community engaged scholarship (especially the scholarly dimensions
    )</li>

	<li>
		Involve community partners from beyond MSU’s campus
  </li>

	<li>
		Be collaboratively undertaken with community partner(s) and a mentor
  </li>

	<li>
		Involve significant, direct interaction between you, the learner, and your community partner
  </li>

	<li>
		Include reflection on communication, collaboration, and partnering skills with a mentor
  </li>

	<li>
		Include critical feedback from the community partner about your collaborative work together
  </li>

	<li>
		Be 60 hours at the minimum and documented in a log of activities (example below)
  </li>
</ul>

<h2>
	On-going Reflection and Feedback from your Mentor
</h2>

<p>
	You are expected to identify a mentor, someone who can serve as a sounding board, reflection partner, and informal advisor for you on your mentored community engagement experience. This person may be your major professor, graduate advisor, field placement supervisor, faculty mentor, member of the University Outreach and Engagement faculty and staff, or a professional from your community setting. Their role is to support you throughout your mentored community engagement experience and reflect with you about your experiences. They are to offer practice ideas and advice as well as serve in a sense-making role. If you do not have a mentor, please talk with the program coordinator who will match you with a mentor who has similar interests and relevant experiences.
</p>

<p>
	When you first meet with your mentor, you may want to clarify:
</p>

<ul>
	<li>
		Availability (e.g., teaching schedules, scheduled travel).
  </li>

	<li>
		Preferred communication styles (e.g., emails, in person meetings, phone calls).
  </li>

	<li>
		Expected frequency of meetings.
  </li>

	<li>
		How to be in touch if something exciting or particularly challenging comes up.
  </li>

	<li>
		Past experiences with community-engaged scholarship.
  </li>

	<li>
		Details about your mentored community engagement experience.
  </li>

	<li>
		Resources that might address anticipated challenges.
  </li>
</ul>

<p>
	At the <strong>end of your experience</strong>, you should gather constructive feedback from your mentor. He or she should comment on the various issues you discussed throughout your mentored community engagement experience, your strengths, and areas for improvement, if any. In the past, most mentors have written a letter or an email for you to include in your written portfolio. Keep in mind, many mentors are busy, so be sure to ask for this letter well in advance of your written portfolio deadline.
</p>

<h2>
	On-going Reflection and Feedback <br class="d-none d-md-block" /> from your Community Partner
</h2>

<p>
	Throughout your collaboration with your community partner, you should be in conversation with them about how your work together is going. Reflecting on what is working well and what might be improved should be part of an on-going conversation or dialogue between you and your community partner.
</p>

<p>
	<strong>At the mid-point</strong> of your mentored community engagement experience, you should ask for critical, reflective feedback from your community partner. More specifically, you should check in with your community partner to compare your expectations and theirs, identify potential adjustments to improve your relationship, and revisit (and revise if necessary) your agreement about the details of your final contributions or project.
</p>

<p>
	We have developed a <strong>Community Partner Feedback Guide</strong> for you to use with your partner to facilitate this mid-point check in conversation. Please share this handout with your community partner at the beginning of your experience and schedule a time to discuss the form’s main points with your community partner at the mid-way point.
</p>

<p>
	At the <strong>end of your experience</strong>, you should gather constructive feedback about your performance, including your strengths and areas for improvement in your community partnership skills. This critical, reflective feedback on your experience may take different forms, depending on the circumstances. In the past, most community partners have written a letter or an email for you to include in your written portfolio. Keep in mind, many community partners are busy, so be sure to ask for this letter well in advance of your written portfolio deadline.
</p>


<hr class="divider" />


<h2>
	Log of Activities
</h2>

<p>
	You are expected to keep an activity log of dates, hours, and activities to document your collaboration with your community partner. Be sure to record the time you spend
</p>

<ul>
	<li>
		preparing for engagement
  </li>

	<li>
		directly engaging with your community partners
  </li>

	<li>
		following-up with your community partners
  </li>
</ul>

<p>
	This log will become part of your written engagement portfolio. See example below:
</p>

<table class="table table-bordered activities-log-table mb-60">
	<caption class="sr-only">
		Log of Activities Table
  </caption>

  <thead>
		<tr>
			<th id="date" scope="col">Date</th>
			<th id="hours" scope="col">Hours</th>
			<th id="log" scope="col">Log of Activities</th>
		</tr>
	</thead>

	<tfoot>
		<tr>
			<th colspan="3">Total Hours</th>
		</tr>
	</tfoot>

	<tbody>
		<tr>
			<td headers="date" id="date1">
				<p>
					Sept 15, 2019</p>
			</td>
			<td headers="date date1">
				<p>
					1 hr</p>
			</td>
			<td headers="date date1">
				<p>
					Met with community partner to brainstorm ideas for upcoming meeting. Partner asked me to look into fun, large group techniques for brainstorming and prioritizing.</p>
			</td>
		</tr>
		<tr>
			<td headers="date" id="date2">
				<p>
					Sept 25, 2019</p>
			</td>
			<td headers="date date2">
				<p>
					2 hrs</p>
			</td>
			<td headers="date date2">
				<p>
					Looked at collaboration techniques books, surfed the web for 5 choices, wrote up summaries to share back with community partner.</p>
			</td>
		</tr>
		<tr>
			<td headers="date" id="date3">
				<p>
					Oct 6, 2019</p>
			</td>
			<td headers="date date3">
				<p>
					1 ½ hrs</p>
			</td>
			<td headers="date date3">
				<p>
					Met with community partner to review techniques choices. Decided on combining two of them. Also decided on final agenda for upcoming big meeting. Divided up roles.</p>
			</td>
		</tr>
		<tr>
			<td headers="date" id="date4">
				<p>
					Oct 9, 2019</p>
			</td>
			<td headers="date date4">
				<p>
					1 hr</p>
			</td>
			<td headers="date date4">
				<p>
					Revised the big meeting agenda, prepared the materials for everyone.</p>
			</td>
		</tr>
		<tr>
			<td headers="date" id="date5">
				<p>
					Oct 23, 2019</p>
			</td>
			<td headers="date date5">
				<p>
					2 hrs</p>
			</td>
			<td headers="date date5">
				<p>
					Attended the big meeting, facilitated part of the large group techniques and took notes for the rest of the meeting.</p>
			</td>
		</tr>
		<tr>
			<td headers="date" id="date6">
				<p>
					Oct 27, 2019</p>
			</td>
			<td headers="date date6">
				<p>
					½ hr</p>
			</td>
			<td headers="date date6">
				<p>
					Wrote up draft notes and sent to community partner for final review.</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>
					&nbsp;</p>
			</td>
			<td>
				<p>
					&nbsp;</p>
			</td>
			<td>
				<p>
					More entries as the project unfolds….</p>
			</td>
		</tr>
	</tbody>
</table>
<p>
	If you are already keeping track of your hours and activities for another purpose (i.e., assistantship, field placement, grant tracking), feel free to include that record of your activities in your portfolio instead of this proposed log of activities.</p>
<p>
	Finally, it’s important to note that most learners far exceed the 60 hours for their projects. That number was chosen as a minimum number. Do not be alarmed if your project unfolds over a longer period of time.</p>
