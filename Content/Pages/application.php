<div class="pt-40 pb-30 pb-md-30">
  <?php include("Views/Shared/Partials/page-banner.php"); ?>
</div>

<h1 class="page-title"><?php echo str_replace("-", " ", ucfirst($page_content)); ?></h1>

<h2>
	Who Should Apply?
</h2>

<p>
	Graduate and professional students from any program whose scholarly and career interests include the following types of activities:
</p>

<ul>
	<li>
		Employing community-based research methods and techniques to address society's problems
  </li>

  <li>
		Providing expertise and assistance to non-profit organizations, government agencies, and industry
  </li>

  <li>
		Involving students in community-based research, service learning, or alternative spring breaks
  </li>

  <li>
		Developing activities or curricula to engage general public audiences in learning</li>
	<li>
		Creating learning experiences and continuing education programs for working professionals
  </li>

  <li>
		Translating health and scientific research findings for general public audiences
  </li>

  <li>
		Involving members of the community in the creative arts, humanities, design, or performance
  </li>
</ul>

<br />

<h2>
	Why Apply?
</h2>

<p>
	Past graduate and professional students say the program has given them an opportunity to:
</p>

<ul>
	<li>
		Prepare for a career as a community-engaged scholar or practitioner
  </li>

	<li>
		Learn about scholarly approaches to community engagement
  </li>

	<li>
		Gain skills for collaborating effectively and respectfully with community partners
  </li>

	<li>
		Share disciplinary knowledge and experiences with other graduate students, faculty, and staff
  </li>

	<li>
		Network with other engaged scholars and practitioners—on campus and nationally
  </li>
</ul>

<hr class="divider"/>

<h2>
	Making Your Application
</h2>

<p class="alert alert-warning d-inline-block">
	Application Deadline: <strong><?php echo $application_deadline; ?></strong>
</p>

<p>
	To apply for the MSU Graduate Certification in Community Engagement, you must
</p>

<ul>
	<li>
		complete an on-line application form
  </li>

  <li>
		arrange to have an on-line nomination form completed by your major professor, faculty adviser, graduate adviser, or other departmental/school person familiar with your professional goals and academic standing
  </li>
</ul>

<p>
	The online application and nomination forms are both due by <strong><?php echo $application_deadline; ?></strong>.
</p>


<hr class="divider"/>

<h2>
	Online Application
</h2>

<p>
	The online application form is composed of multiple choice questions about contact information, degree program, career aspirations, photo release, and demographic data. Three open-ended questions address biographical information, your community engaged scholarship, and learning interests. Those questions are as follows:
</p>

<p>
	<strong>Biographical Information:</strong> In 250 words (or less), please introduce yourself to the other participants and seminar conveners by sharing: your motivation for community-engaged scholarship, your educational background, and any significant community engagement experiences. (Note: this information will be shared with fellow participants).
</p>

<p>
	<strong>Your Community-Engaged Scholarship:</strong> In 250-500 words (or less), tell us about your interest in community-engaged scholarship. What social issues interest you? What types of communities would you like to collaborate with? If you have an idea about your mentored community engagement experience, please describe it here.
</p>

<p>
	<strong>Learning Interests:</strong> In 250 words (or less), tell us about your learning interests. What specifically do you hope the MSU Graduate Certification in Community Engagement can do to enhance your community-engaged scholarship and practice?
</p>

<p>
	Applicants may paste their open-ended responses into the form.
</p>

<p>
	<a href="https://msu.co1.qualtrics.com/jfe/form/SV_2mC1sqmNOV90mFL" <?php echo $external ?> class="btn btn-theme btn-theme-accent" target="_blank">Complete the Online Application Form</a>
</p>

<hr class="divider">

<h2>
	Online Nomination Form
</h2>

<p class="alert alert-warning d-inline-block">
	Nomination Deadline: <strong><?php echo $application_deadline; ?></strong>
</p>

<p>
	Graduate Certification in Community Engagement applicants should arrange to have their major professor, faculty adviser, graduate adviser, or other departmental/school person familiar with your professional goals and academic standing complete the online application form. Only one nomination is required for each applicant.
</p>

<p>
	The on-line nomination form is composed of 6 rating questions and 1 open-ended question that asks, "In no more than 150 words, please describe how the applicant would benefit from participation in the MSU Graduate Certification in Community Engagement. What specific areas of learning and professional development do you hope this program addresses for the applicant?"
</p>

<p>
	Nominators may paste their open-ended responses into the form.
</p>

<p>
	<a href="https://msu.co1.qualtrics.com/jfe/form/SV_cHNBT29xGAyGxJH" <?php echo $external ?> class="btn btn-theme btn-theme-primary" target="_blank">Complete the Online Nomination Form</a>
</p>

<hr>

<p class="small">
	<em>
    In limited circumstances, interested students can be admitted after the application deadline. Please contact the program coordinator for more details about this exception.
  </em>
</p>
