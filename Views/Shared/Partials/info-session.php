<div class="alert alert-dismissible fade show p-0">

  <button type="button" class="close mt-60" data-dismiss="alert" aria-label="Close" style="z-index: 100; mix-blend-mode: multiply; opacity: 0.3;">
    <span aria-hidden="true">&times;</span>
  </button>

<hr />

<section class="container info-session">

  <div class="row character <?php echo $people[$random_person[4]];?>">
    <div class="col-4 col-sm-3 character-box">
      <div class="person-circle"></div>
    </div>

    <div class="col-8 col-sm-8 color-box-35 content">
      <h2>Info Session</h2>
      <p>
        Learn about program requirements, the online application process, and examples from past students in the program.
      </p>

      <p>
        <strong class="date">
          Thursday,<br class="d-inline d-sm-none" /> September 1, 2022
        </strong>

        <br />

        <span class="info">
          &nbsp; 1:30 – 3:30pm &nbsp;
          <span class="d-none d-sm-inline text-muted">||</span>
          <br class="d-inline d-sm-none" />
          &nbsp; Zoom Meeting
        </span>
      </p>

      <p>
        <a href="about" <?php if ($page_content == "about"){echo 'class="d-none"';}?>>Learn more about the Graduate Certification</a>
      </p>

      <p>
        <strong><a href="#" class="btn btn-theme btn-theme-primary">Register Now!</a></strong>
      </p>
    </div>
  </div>
</section>
</div>
