<?php
$site_title = "Graduate Certification in Community Engagement";

$keywords = "outreach engagement scholar scholarship community development responsive build capacity building collaborative collaboration michigan state university grad certificate notation";

$author = "University Outreach and Engagement - Communication and Information Technology";

$description = "Michigan State University&#39;s Graduate Certification in Community Engagement prepares students for academic and professional careers that integrate scholarship with community engagement.";

$main_phone = "(517) 353-8977";

$application_deadline = "Friday, September 9, 2022";
$info_session_date = "Thursday, September 1, 2022";

$people = array(
  "Austen",
  "Bryce",
  "Carson",
  "Darcy",
  "Ellis",
  "Faye",
  "Gael",
  "Hollis",
  "Inez",
  "Jaden",
  "Kai",
  "Linden"
);

$random_person = array_rand($people, 5);

$external = 'data-toggle="tooltip" data-placement="right" title="External Link"';
$pdf = 'data-toggle="tooltip" data-placement="right" title="PDF"';

?>
