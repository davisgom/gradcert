<aside class="page-banner behind">
  <div class="people-bg">
    <div class="people">
      <div class="character character-box <?php echo $people[$random_person[0]];?>">
        <div class="person"></div>
        <div class="circle"></div>
      </div>

      <div class="character character-box d-none d-sm-block <?php echo $people[$random_person[1]];?>">
        <div class="person"></div>
        <div class="circle"></div>
      </div>

      <div class="character character-box d-none d-md-block <?php echo $people[$random_person[2]];?>">
        <div class="person"></div>
        <div class="circle"></div>
      </div>
    </div>
  </div>
</aside>
