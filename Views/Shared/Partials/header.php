<div id="skip-nav" class="sr-only sr-only-focusable">
    <ul>
        <li><a href="#Main">Skip to the Content</a></li>
        <li><a href="#Footer">Skip to the Footer</a></li>
    </ul>
</div>

<header>
  <?php include("msu-masthead.php"); ?>

  <section class="site-masthead">
    <div class="container">
      <h1><a href="home" class="logo text-hide"><?php echo $site_title; ?></a></h1>

      <?php include("nav.php"); ?>
    </div>
  </section>
</header>
