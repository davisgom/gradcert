<button class="menu-btn btn btn-light d-md-block d-lg-none d-print-none" onclick="navToggle()" onkeydown="navToggle()">
  <span class="menu-icon">
    <i class="fas fa-bars"></i> <br /> <span>Menu</span>
  </span>
  <span class="search-icon">
    <i class="fas fa-search fa-lg"></i>
  </span>
</button>

<section id="siteNav" class="overlay">
  <div class="overlay-content">
    <div class="container">
      <a class="closebtn d-lg-none" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>

      <div id="MSUSearchTool" class="no-fill d-lg-none" role="search">
    			<?php include("msu-search.php"); ?>
    	</div>

      <nav class="row">
        <h1 class="sr-only">Site Navigation</h1>
        <ul class="nav nav-pills nav-fill" role="navigation">
            <li>
              <a href="home">
                Home
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "about") {echo 'class="active"';} ?> href="about">
                About
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "requirements") {echo 'class="active"';} ?> href="requirements">
                Requirements
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "application") {echo 'class="active"';} ?> href="application">
                Application
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "resources") {echo 'class="active"';} ?> href="resources">
                Resources
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "contact") {echo 'class="active"';} ?> href="contact">
                Contact
              </a>
            </li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="container closebtn-bottom d-lg-none">
		<a class="closebtn" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>
	</div>
</section>
