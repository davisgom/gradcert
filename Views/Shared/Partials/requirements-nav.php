<nav class="requirements-nav">
	<span class="parent">Requirements</span>
	<ul>
		<li>
      <a <?php if ($page_content == "competencies") {echo 'class="active"';} ?> href="competencies" >
        Competencies
      </a>
    </li>
		<li>
      <a <?php if ($page_content == "community-experience") {echo 'class="active"';} ?> href="community-experience" >
        Community Experience
      </a>
    </li>
		<li>
      <a <?php if ($page_content == "engagement-portfolio") {echo 'class="active"';} ?> href="engagement-portfolio" >
        Engagement Portfolio
      </a>
    </li>
	</ul>
</nav>
