<div class="alert alert-dismissible fade show p-0">

  <button type="button" class="close mt-60" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>

<hr />

<section class="container info-session">

  <div class="row character <?php echo $people[$random_person[4]];?>">
    <div class="col-4 col-sm-3 character-box">
      <div class="person-circle"></div>
    </div>

    <div class="col-8 col-sm-8 color-box-35 content">
      <h2>Applications Due</h2>

        <p>
        <strong class="date">
          <?php echo $application_deadline; ?>, 5:00 pm
        </strong>
      </p>

      <p>
        <a href="about" <?php if ($page_content == "about"){echo 'class="d-none"';}?>>Learn more about the Graduate Certification</a>
      </p>

      <p>
        <strong><a href="application" class="btn btn-theme btn-theme-primary">Apply Now!</a></strong>
      </p>
    </div>
  </div>
</section>
</div>
