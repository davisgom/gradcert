# GradCert Prototype

This is the prototype for the 2022 Redesign of the Graduate Certification in Community Engagement website.  

You can view the prototype online at: [https://gradcert.mdavis.in](https://gradcert.mdavis.in)  
